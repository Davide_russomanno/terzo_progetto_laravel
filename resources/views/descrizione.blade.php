<x-layout>
    <div class="contenitore d-flex justify-content-center">
<div class="cont-specchio">
    <div class="container my-5">
    <div class="row">
        <div class="col-12 col-md-6">
            <img src="{{ $app['img-2'] }}" alt="{{ $app['nome'] }}" class="img-fluid">
        </div>
        <div class="col-12 col-md-6 text">
            <p>{{ $app['nome'] }}</p>
            <p>Genere: {{ $app['genere'] }}</p>
            <p>Dettagli: {{ $app['dettagli'] }}</p>
            <p>Prezzo: {{ $app['prezzo'] }}</p>
            <p>Anno: {{ $app['annoProd'] }}</p>
        </div>
    </div>
</div>
</div>
</div>

</x-layout>
