<x-layout>

    <div class="container-fluid">
        <div class="row">
            
            @foreach($apps as $app)
            <div class="col-12 col-md-3 my-5 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="{{ $app['img'] }}" class="card-img-top" alt="{{ $app['nome'] }}">
                <div class="card-body card-color">
                    <h5 class="card-title">{{ $app['nome'] }}</h5>
                    <p class="card-text">Genere: {{ $app['genere'] }}</p>
                    <p class="card-text">Dettagli: {{ substr($app['dettagli'], 0, 50) }}...</p>
                    <a href="{{ route('prezzo', ['prezzo' => $app['prezzo']]) }}" class='text-a'>
                        <p class="card-text">Prezzo: {{ $app['prezzo'] }}</p>
                    </a>
                    <p class="card-text">Anno: {{ $app['annoProd'] }}</p>
                    <a href="{{ route('descrizione', ['id' => $app['id']]) }}" class="btn btn-color">Scopri</a>
                </div>
            </div>
        </div>
            @endforeach
        </div>
    </div>


</x-layout>