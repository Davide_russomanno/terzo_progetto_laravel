<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;


Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/descrizione/{id}', [PublicController::class, 'descrizione'])->name('descrizione');
Route::get('/prezzo/{prezzo}', [PublicController::class, 'prezzo'])->name('prezzo');
