<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{

    public $apps = [
        [
            'id' => '1',
            'img' => '/img/tetris.webp',
            'img-2' => '/img/tetris.webp',
            'nome' => 'Tetris',
            'genere' => 'Rompicapo',
            'dettagli' => 'Tetris (in russo: Тетрис, sincrasi di тетрамино, "tetramino", e теннис, "tennis") è un videogioco di logica e ragionamento inventato da Aleksej Leonidovič Pažitnov nel 1984, mentre lavorava al centro di calcolo dell Accademia delle Scienze dell URSS di Mosca.',
            'prezzo' => 'Gratis',
            'annoProd' => 1984
        ],
        [
            'id' => '2',
            'img' => '/img/pou.png',
            'img-2' => '/img/pou-2.png',
            'nome' => 'Pou',
            'genere' => 'Simulatore di vita',
            'dettagli' => 'Lo scopo del videogioco è di far crescere e accudire un animaletto, precisamente un alieno, chiamata "Pou", inizialmente di piccole dimensioni. Il giocatore ha a disposizione diversi ambienti:

            La cucina, in cui potrà far crescere Pou nutrendolo con cibi di vario genere prelevabili dal suo frigorifero;
            La sala giochi, dove potrà far divertire Pou con diversi mini-giochi, che consentono anche lottenimento di monete spendibili nel gioco stesso in prodotti di vario tipo;
            <br>
            La stanza da letto, dove far riposare Pou e dove, allinterno dellarmadio, cambiargli colore, aspetto fisico, vestiti e trucco;
            <br>
            Un bagno, che include saponetta, doccia e negozio, dove si possono comprare saponette con fragranze assortite, colore della doccia e altre cose;
            <br>
            Il laboratorio, dove curare Pou. Difatti, se viene fatto ingrassare troppo o se viene lasciato sporco, Pou si ammala, e necessita quindi di pozioni dimagranti o di medicine per la salute;
            <br>
            Il giardino, dove Pou può giocare e innaffiare dei fiori personalizzabili, "chiamare con un cellulare" (nel senso cercare amici) e modificare laspetto esterno della sua piccola casa.',
            'prezzo' => 'Gratis',
            'annoProd' => 2012 
        ],
        [
            'id' => '3',
            'img' => '/img/minecraft.png',
            'img-2' => '/img/minecraft.png',
            'nome' => 'Minecraft',
            'genere' => 'Sandbox',
            'dettagli' => 'Minecraft è un gioco dove si scava (mine) e costruisce (craft) con diversi tipi di blocchi 3D, all interno di un grande mondo fatto da diversi tipi di terreni e habitat da esplorare. In questo mondo il sole sorge e tramonta, si va al lavoro, si raccolgono materiali e si costruiscono utensili.',
            'prezzo' => '€6.99',
            'annoProd' => 2017 
        ],
        [
            'id' => '4',
            'img' => '/img/amongus.jpg',
            'img-2' => '/img/amongus.jpg',
            'nome' => 'Among Us',
            'genere' => 'Gioco di sopravvivenza',
            'dettagli' => 'Among Us è un gioco in multiplayer da 4 a 10 giocatori che si ritrovano in una location a tema spaziale (base o astronave) in un gruppo con all interno nascosti degli impostori, da 1 a 3. Lo scopo del gioco è di scoprire gli impostori se si è membri del gruppo (crewmate) o uccidere tutti se si è tra i cattivi',
            'prezzo' => 'Gratis',
            'annoProd' => 2018 
        ],
        [
            'id' => '5',
            'img' => '/img/callofduty.jpg',
            'img-2' => '/img/callofduty-2.jpg',
            'nome' => 'Call of Duty: Mobile',
            'genere' => 'Sparatutto',
            'dettagli' => 'CALL OF DUTY® MOBILE offre gaming in HD sul tuo telefono con controlli intuitivi e personalizzabile, chat vocale e testuale con gli amici, e straordinari comparti grafico e sonoro. Prova questo iconico franchise sul tuo telefono e gioca dove vuoi, quando vuoi.',
            'prezzo' => 'Gratis',
            'annoProd' => 2019 
        ],
        [
            'id' => '6',
            'img' => '/img/subway.jpg',
            'img-2' => '/img/subway-2.jpg',
            'nome' => 'Subway Surfers',
            'genere' => 'Endless runner',
            'dettagli' => 'Subway Surfers è un app in cui dovete aiutare Jake a fuggire lungo i binari ferroviari, pieni di ostacoli e treni che sfrecciano contro di voi.',
            'prezzo' => 'Gratis',
            'annoProd' => 2012 
        ],
        [
            'id' => '7',
            'img' => '/img/league.jpg',
            'img-2' => '/img/league-2.jpg',
            'nome' => 'League of Legends: Wild Rift',
            'genere' => 'Action RPG',
            'dettagli' => 'League of Legends: Wild Rift è un MOBA (multiplayer online battle arena) multigiocatore con visuale isometrica. I giocatori possono affrontarsi in partite normali o classificate cooperando per distruggere le difese nemiche (chiamate "torri") e raggiungere il Nexus.',
            'prezzo' => 'Gratis',
            'annoProd' => 2020 
        ],
        [
            'id' => '8',
            'img' => '/img/asphalt.png',
            'img-2' => '/img/asphalt.png',
            'nome' => 'Asphalt 9: Legends',
            'genere' => 'Corse Auto',
            'dettagli' => 'In Asphalt 9: Legends troverai una scuderia di auto da sogno unica nel suo genere, con bolidi di costruttori rinomati come Ferrari, Porsche, Lamborghini, W Motors e altri ancora. Metti il piede sull acceleratore ed esegui acrobazie in ambientazioni dinamiche e reali, in gare per giocatore singolo e multiplayer.',
            'prezzo' => 'Gratis',
            'annoProd' => 2018 
        ],
        [
            'id' => '9',
            'img' => '/img/monument.webp',
            'img-2' => '/img/monument.webp',
            'nome' => 'Monument Valley',
            'genere' => 'rompicapo',
            'dettagli' => 'Monument Valley è un esplorazione surreale attraverso fantastici elementi architettonici e geometrie impossibili. Guida la silenziosa principessa Ida attraverso misteriosi monumenti, scoprendo sentieri nascosti, svelando illusioni ottiche e battendo in astuzia l enigmatico popolo dei corvi.',
            'prezzo' => '€3.99',
            'annoProd' => 2014 
        ],
        [
            'id' => '10',
            'img' => '/img/room.webp',
            'img-2' => '/img/room.webp',
            'nome' => 'The Room 1 and 2',
            'genere' => 'Horror',
            'dettagli' => 'I giocatori esplorano una complessa casa delle bambole e risolvono i puzzle segreti.',
            'prezzo' => '€0.99',
            'annoProd' => 2017 
        ],
        [
            'id' => '11',
            'img' => '/img/diablo.jpg',
            'img-2' => '/img/diablo.jpg',
            'nome' => 'Diablo Immortal',
            'genere' => 'Azione',
            'dettagli' => 'Diablo Immortal™ è il nuovo gioco della serie di GdR d azione di Blizzard Entertainment che ha definito il genere, ed è ambientato tra gli eventi di Diablo® II: Lord of Destruction® e Diablo III®. Scontrati con armate di demoni, raccogli bottini epici e ottieni poteri inimmaginabili.',
            'prezzo' => 'Gratis',
            'annoProd' => 2022 
        ],
        [
            'id' => '12',
            'img' => '/img/fifa.webp',
            'img-2' => '/img/fifa.webp',
            'nome' => 'FIFA Calcio',
            'genere' => 'Calcio',
            'dettagli' => 'Federazione internazionale di calcio dell Association è la federazione internazionale che governa gli sport del calcio, del calcio a 5 e del beach soccer.',
            'prezzo' => 'Gratis',
            'annoProd' => 1904 
        ],
    ];

    public function home() {
        return view('welcome', ['apps' => $this->apps]);
    }

     public function descrizione($id){
        
         foreach($this->apps as $app){
             if($app['id'] == $id){
                 return view('descrizione', ['app' => $app]);
             }
         }
    }

    public function prezzo($prezzo){

        $prodPrezzo = [];
        
        foreach($this->apps as $prezzoApp){
            if($prezzoApp['prezzo'] == $prezzo){
                $prodPrezzo[] = $prezzoApp;
            }
        }
        return view('prezzo', ['prezzo' => $prodPrezzo]);
   }
}
